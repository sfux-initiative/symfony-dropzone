# Symfony UX Dropzone

Symfony UX Dropzone is a Symfony bundle providing light dropzones for file inputs
in Symfony Forms. It relies on the tools of the Symfony UX initiative to work.

It allows visitors to drag and drop files into a container instead of having
to browse their computer for a file.

Symfony UX Dropzone is currently considered **experimental**.

## Installation

Symfony UX Dropzone requires PHP 7.1+ and Symfony 5.0+.

You can install this bundle using Composer and Symfony Flex:

```sh
composer require symfony/ux-dropzone

# Don't forget to install the JavaScript dependencies as well and compile
yarn install --force
yarn dev
```

## Usage

The most common usage of Symfony UX Dropzone is to use it as a drop-in replacement of 
the native FileType class:

```php
// ...
use Symfony\UX\Dropzone\Form\DropzoneType;

class CommentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ...
            ->add('photo', DropzoneType::class)
            // ...
        ;
    }

    // ...
}
```

### Customizing the design

Symfony UX Dropzone provides a default stylesheet in order to ease usage. However you can
disable it in order to add your own design if you wish.

In your assets controllers configuration file (`assets/controllers.json`), you can should 
have something like the following:

```json
{
    "controllers": {
        "@symfony/ux-dropzone": {
            "dropzone": {
                "enabled": true,
                "webpackMode": "eager",
                "autoimport": {
                    "@symfony/ux-dropzone/src/style.css": true
                }
            }
        }
    },
    "entrypoints": []
}
```

If you wish to disable the default CSS stylesheet in order to implement your own, you can
update the dedicated `autoimport` key:

```json
{
    "controllers": {
        "@symfony/ux-dropzone": {
            "dropzone": {
                "enabled": true,
                "webpackMode": "eager",
                "autoimport": {
                    "@symfony/ux-dropzone/src/style.css": false
                }
            }
        }
    },
    "entrypoints": []
}
```

> *Note*: you should put the value to `false` and not remove the line so that Symfony Flex
> won't try to add the line again in the future.

### Reacting to dropzone changes

Symfony UX Dropzone dispatches JavaScript events when the dropzone changes or is cleared.
To react to these events, you can use a custom Stimulus controller:

```js
// mydropzone_controller.js

import { Controller } from 'stimulus';

export default class extends Controller {
    connect() {
        this.element.addEventListener('dropzone:change', this._onChange);
        this.element.addEventListener('dropzone:clear', this._onClear);
    }

    disconnect() {
        // You should always remove listeners when the controller is disconnected to avoid side-effects
        this.element.removeEventListener('dropzone:change', this._onChange);
        this.element.removeEventListener('dropzone:clear', this._onClear);
    }

    _onChange(event) {
        // The input just changed
    }

    _onClear(event) {
        // The input has just been cleared
    }
}
```

Then in your form configuration, add your controller in addition to the default one
using the `DropzoneType` options:

```php
// ...
use Symfony\UX\Dropzone\Form\DropzoneType;

class CommentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ...
            ->add('photo', DropzoneType::class, [
                'attr' => ['data-controller' => 'mydropzone'],
            ])
            // ...
        ;
    }

    // ...
}
```

## Backward Compatibility promise

This bundle aims at following the same Backward Compatibility promise as the Symfony framework:
[https://symfony.com/doc/current/contributing/code/bc.html](https://symfony.com/doc/current/contributing/code/bc.html)

However it is currently considered **experimental**.

## Run tests

### PHP tests

```sh
php vendor/bin/phpunit
```

### JavaScript tests

```sh
cd assets
yarn test
```
