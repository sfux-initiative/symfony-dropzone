/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

'use strict';

import { Application } from 'stimulus';
import { getByTestId, waitFor } from '@testing-library/dom';
import user from '@testing-library/user-event';
import { clearDOM, mountDOM } from '@symfony/stimulus-testing';
import DropzoneController from '../dist/controller';

const startStimulus = () => {
    const application = Application.start();
    application.register('dropzone', DropzoneController);
};

describe('DropzoneController', () => {
    let container;

    beforeEach(() => {
        container = mountDOM(`
            <div class="dropzone-container" data-controller="dropzone" data-testid="container"> 
                <input type="file"
                       style="display: none"
                       data-target="dropzone.input"
                       data-testid="input" />
        
                <div class="dropzone-placeholder" 
                     data-target="dropzone.placeholder" 
                     data-testid="placeholder">
                    Placeholder
                </div>
        
                <div class="dropzone-preview"
                     data-target="dropzone.preview"
                     data-testid="preview"
                     style="display: none">
                     
                    <button type="button"
                            class="dropzone-preview-button"
                            data-target="dropzone.previewClearButton"
                            data-testid="button"></button>
        
                    <div class="dropzone-preview-image"
                         data-target="dropzone.previewImage"
                         data-testid="preview-image"
                         style="display: none"></div>
        
                    <div class="dropzone-preview-filename"
                         data-target="dropzone.previewFilename" 
                         data-testid="preview-filename"></div>
                </div>
            </div>
        `);
    });

    afterEach(() => {
        clearDOM();
    });

    it('connect', async () => {
        expect(getByTestId(container, 'input')).toHaveStyle({ display: 'none' });

        // Launch Stimulus
        startStimulus();

        // The controller should display the input on connect
        await waitFor(() => expect(getByTestId(container, 'input')).toHaveStyle({ display: 'block' }));
    });

    it('clear', async () => {
        startStimulus();
        await waitFor(() => expect(getByTestId(container, 'input')).toHaveStyle({ display: 'block' }));

        // Attach a listener to ensure the event is dispatched
        let dispatched = false;
        getByTestId(container, 'container').addEventListener('dropzone:clear', () => (dispatched = true));

        // Manually show preview
        getByTestId(container, 'input').style.display = 'none';
        getByTestId(container, 'placeholder').style.display = 'none';
        getByTestId(container, 'preview').style.display = 'block';

        // Click the clear button
        getByTestId(container, 'button').click();

        await waitFor(() => expect(getByTestId(container, 'input')).toHaveStyle({ display: 'block' }));
        await waitFor(() => expect(getByTestId(container, 'placeholder')).toHaveStyle({ display: 'block' }));
        await waitFor(() => expect(getByTestId(container, 'preview')).toHaveStyle({ display: 'none' }));

        // The event should have been dispatched
        expect(dispatched).toBe(true);
    });

    it('file chosen', async () => {
        startStimulus();
        await waitFor(() => expect(getByTestId(container, 'input')).toHaveStyle({ display: 'block' }));

        // Attach a listener to ensure the event is dispatched
        let dispatched = null;
        getByTestId(container, 'container').addEventListener('dropzone:change', (event) => (dispatched = event));

        // Select the file
        const input = getByTestId(container, 'input');
        const file = new File(['hello'], 'hello.png', { type: 'image/png' });

        user.upload(input, file);
        expect(input.files[0]).toStrictEqual(file);

        // The dropzone should be in preview mode
        await waitFor(() => expect(getByTestId(container, 'input')).toHaveStyle({ display: 'none' }));
        await waitFor(() => expect(getByTestId(container, 'placeholder')).toHaveStyle({ display: 'none' }));

        // The event should have been dispatched
        expect(dispatched).not.toBeNull();
        expect(dispatched.detail).toStrictEqual(file);
    });
});
